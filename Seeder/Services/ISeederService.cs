﻿using System.Threading.Tasks;

namespace Seeder.Services
{
    public interface ISeederService
    {
        Task Seed(int count);
    }
}