﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using recruitment_bv.Services;

namespace recruitment_bv.Controllers
{
    public class ItemController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemController(IItemService itemService)
        {
            _itemService = itemService;
        }
        
        public async Task<IActionResult> GetAllItems() => Ok(await _itemService.GetAll());
    }
}