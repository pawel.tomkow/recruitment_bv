﻿using System.Threading.Tasks;
using recruitment_bv.Models;

namespace recruitment_bv.Services
{
    public interface ISwordService
    {
        Task<Sword> GetAll();
        Task<Sword> Get(int id);

        Task Add(Sword sword);
    }
}