﻿using System.ComponentModel.DataAnnotations;

namespace recruitment_bv.Models
{
    public class Sword : Item
    {
        public int Id { get; set; }
        [Required] private string Name { get; set; }
        [Required] private float AttackValue { get; set; }
    }
}