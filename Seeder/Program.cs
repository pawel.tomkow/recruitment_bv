﻿using System;
using Seeder.Services;

namespace Seeder
{
    class Program
    {
        // liczba danych do seedowania
        public const int X = 100;
        
        /// <summary>
        /// More https://www.rabbitmq.com/dotnet-api-guide.html
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("Hello Blue Veery!");
            
            ISeederService seeder = new SeederService();
            seeder.Seed(X);
        }
    }
}