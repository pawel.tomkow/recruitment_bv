﻿using System.Collections.Generic;
using System.Threading.Tasks;
using recruitment_bv.Models;

namespace recruitment_bv.Services
{
    public interface IItemService
    {
        Task<IEnumerable<Item>> GetAll();
    }
}